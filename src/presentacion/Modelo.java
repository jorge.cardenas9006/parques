/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacion;

import javax.sound.midi.MidiDevice.Info;
import javax.swing.JFileChooser;

/**
 *
 * @author jorge cardenas
 */
public class Modelo {
    private Vista ventanaPrincipal;
    
    public void iniciar(){
        getVentanaPrincipal().setSize(645,660);
        getVentanaPrincipal().setVisible(true);
    }

    public Vista getVentanaPrincipal() {
        if(ventanaPrincipal == null){
            ventanaPrincipal = new Vista(this);
        }
        return ventanaPrincipal;
    }
    
    
}
